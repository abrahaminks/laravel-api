<?php

namespace Database\Factories;

use App\Models\ContactoModel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ContactoFactory extends Factory
{

    protected $model = ContactoModel::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => fake()->name(),
            'apellido' => fake()->lastName(),
            'empresa' => fake()->company(),
        ];
    }

    // public function suspended(): Factory
    // {
    //     return $this->state(function (array $attributes) {
    //         return [
    //             'activo' => 'no',
    //         ];
    //     });
    // }
}
