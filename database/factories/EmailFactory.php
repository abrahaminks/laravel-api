<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\EmailModel;
use App\Models\ContactoModel;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class EmailFactory extends Factory
{

    protected $model = EmailModel::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $idsContacto = ContactoModel::all()->map(function (ContactoModel $contacto) {
            return $contacto->id;
        });

        return [
            'email' => fake()->unique()->email(),
            'tipo' => 'Personal',
            'proveedor' => fake()->company(),
            'contacto_id' => fake()->randomElement($idsContacto)
        ];
    }
}
