<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\TelefonoModel;
use App\Models\ContactoModel;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class TelefonoFactory extends Factory
{
    protected $model = TelefonoModel::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $idsContacto = ContactoModel::all()->map(function (ContactoModel $contacto) {
            return $contacto->id;
        });
        return [
            'telefono' => fake()->phoneNumber(),
            'lada' => fake()->randomNumber(2,false),
            'tipo' => 'Personal',
            'contacto_id'=>fake()->randomElement($idsContacto)
        ];
    }
}
