<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\DireccionModel;
use App\Models\ContactoModel;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class DireccionFactory extends Factory
{
    protected $model = DireccionModel::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $idsContacto = ContactoModel::all()->map(function (ContactoModel $contacto) {
            return $contacto->id;
        });

        return [
            'calle' => fake()->streetName(),
            'numero_externo' => fake()->randomNumber(5, false),
            'numero_interno' => fake()->randomNumber(5, false),
            'colonia' => fake()->secondaryAddress(),
            'municipio' => fake()->city(),
            'estado' => fake()->state(),
            'pais' => fake()->country(),
            'codigo_postal' => fake()->postcode(),
            'referencias' => fake()->text(),
            'contacto_id' => fake()->randomElement($idsContacto)
        ];
    }
}
