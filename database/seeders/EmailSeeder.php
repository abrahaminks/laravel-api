<?php

namespace Database\Seeders;

use App\Models\EmailModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EmailModel::factory()
            ->count(2000)
            //->sequence(['tipo'=>'Personal'],['tipo'=>'Empresarial'])
            ->create();
    }
}
