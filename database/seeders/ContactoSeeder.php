<?php

namespace Database\Seeders;

use App\Models\ContactoModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContactoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ContactoModel::factory()
            ->count(1000)
            ->create();
    }
}
