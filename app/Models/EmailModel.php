<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\EmailFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EmailModel extends Model
{
    use HasFactory;

    protected $table = 'email';

    protected $fillable=[
        'email',
        'tipo',
        'proveedor',
    ];    

    protected static function newFactory(): Factory
    {
        return EmailFactory::new();
    }

    //Relacion a uno
    public function contacto():BelongsTo{
        return $this->belongsTo(ContactoModel::class);
    }
}
