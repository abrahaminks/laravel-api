<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\ContactoFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ContactoModel extends Model
{
    use HasFactory;

    protected $table = 'contacto';

    protected $fillable=[
        'nombre',
        'apellido',
        'empresa',
    ];

    protected static function newFactory(): Factory
    {
        return ContactoFactory::new();
    }

    //Relacion OneToMany
    public function telefonos():HasMany{
        return $this->hasMany(TelefonoModel::class);
    }

    public function emails():HasMany{
        return $this->hasMany(EmailModel::class);
    }
    
    public function direcciones():HasMany{
        return $this->hasMany(DireccionModel::class);
    }
}
