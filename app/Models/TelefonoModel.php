<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\TelefonoFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TelefonoModel extends Model
{
    use HasFactory;

    protected $table = 'telefono';

    protected $fillable=[
        'lada',
        'telefono',
        'tipo',
        'contacto_id',
    ];

    protected static function newFactory(): Factory
    {
        return TelefonoFactory::new();
    }

    //Relacion a uno
    public function contacto():BelongsTo{
        return $this->belongsTo(ContactoModel::class);
    }
}
