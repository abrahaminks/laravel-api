<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\DireccionFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DireccionModel extends Model
{
    use HasFactory;

    protected $table = 'direccion';

    protected $fillable=[
        'calle',
        'numero_externo',
        'numero_interno',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'codigo_postal',
        'referencias',
    ];

    protected static function newFactory(): Factory
    {
        return DireccionFactory::new();
    }

    //Relacion a uno
    public function contacto():BelongsTo{
        return $this->belongsTo(ContactoModel::class);
    }
}
