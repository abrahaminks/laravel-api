<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactoModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ContactoController extends Controller
{

    public function create(Request $req){
        
        $validator = Validator::make($req->all(), [
            'nombre'=>'max:255|required',
            'apellido'=>'max:255|required',
            'empresa'=>'max:255|required',
        ]);

        $contacto = ContactoModel::create([
            'nombre'=>$req->nombre,
            'apellido'=>$req->apellido,
            'empresa'=>$req->empresa
        ]);

        if(!$contacto){
            $data = [
                "message"=>"Error al crear usuario",
                "status"=>500
            ];

            return response()->json($data,500);
        }

        $data = [
            "contacto"=>$contacto,
            "status"=>200
        ];

        return response()->json($data,200);
    }
    
    public function readAll(){
        $data = [
            "contactos" => DB::table('contacto')
                            ->leftJoin('telefono', 'telefono.contacto_id', '=', 'contacto.id')
                            ->leftJoin('email', 'email.contacto_id', '=', 'contacto.id')
                            ->leftJoin('direccion', 'direccion.contacto_id', 'contacto.id')
                            ->select('contacto.*', 
                                    'telefono.lada', 
                                    'telefono.telefono', 
                                    'email.email', 
                                    'email.proveedor',
                                    'direccion.calle',
                                    'direccion.numero_externo',
                                    'direccion.numero_interno',
                                    'direccion.colonia',
                                    'direccion.municipio',
                                    'direccion.estado',
                                    'direccion.pais',
                                    'direccion.codigo_postal'
                                    )
                            ->orderBy('updated_at', 'desc')
                            ->orderBy('created_at', 'desc')
                            ->get(),
                            // ->simplePaginate(15),
            // "contactos" => ContactoModel::all()->telefonos(),
            "status" => 200,
        ];
        return response()->json($data, 200);
    }
    
    public function readAllPaged(){
        $data = [
            "contactos" => DB::table('contacto')
                            ->leftJoin('telefono', 'telefono.contacto_id', '=', 'contacto.id')
                            ->leftJoin('email', 'email.contacto_id', '=', 'contacto.id')
                            ->leftJoin('direccion', 'direccion.contacto_id', 'contacto.id')
                            ->select('contacto.*', 
                                    'telefono.telefono', 
                                    'email.email', 
                                    'direccion.calle',
                                    'direccion.numero_externo',
                                    'direccion.numero_interno',
                                    'direccion.colonia',
                                    'direccion.municipio',
                                    'direccion.estado',
                                    'direccion.pais',
                                    'direccion.codigo_postal'
                                    )
                            ->orderBy('nombre')
                            ->orderBy('apellido')
                            ->simplePaginate(15),
                            // ->get(),
            // "contactos" => ContactoModel::all()->telefonos(),
            "status" => 200,
        ];
        return response()->json($data, 200);
    }
    
    public function read($id){

        $contacto = ContactoModel::find($id);

        if(!$contacto){
            $data = [
                "message" => "No se encontro el usuario",
                "status" => 400,
            ];
            
            return response()->json($data, 400);
        }else{
            $contacto = DB::table('contacto')
                        ->leftJoin('telefono', 'telefono.contacto_id', '=', 'contacto.id')
                        ->leftJoin('email', 'email.contacto_id', '=', 'contacto.id')
                        ->leftJoin('direccion', 'direccion.contacto_id', 'contacto.id')
                        ->select('contacto.*', 
                                'telefono.telefono', 
                                'email.email', 
                                'direccion.calle',
                                'direccion.numero_externo',
                                'direccion.numero_interno',
                                'direccion.colonia',
                                'direccion.municipio',
                                'direccion.estado',
                                'direccion.pais',
                                'direccion.codigo_postal')
                        ->where('contacto.id','=', $id)
                        ->get();
        }

        $data = [
            "contacto" => $contacto,
            "status" => 200,
        ];
        
        return response()->json($data, 200);
    }
    
    public function update(Request $req, $id){

        $contacto = ContactoModel::find($id);

        if(!$contacto){
            $data = [
                'message' => 'No se encontro el contacto con ese ID',
                'status' => 400
            ];

            return response()->json($data,400);
        }

        $validator = Validator::make($req->all(), [
            'nombre' => 'required:max:255',                     //Maximo de caracteres
            'apellido' => 'required:max:255',
            'empresa' => 'required',         // Formato EMAIL y UNICO por USUARIO
        ]);

        if($validator->fails()){
            $data = [
                'message'=> 'Error en validacion de datos',
                'errors'=> $validator->errors(),
                'status'=> 400
            ];

            return response()->json($data, 400);
        }

        $contacto->nombre = $req->nombre;
        $contacto->apellido = $req->apellido;
        $contacto->empresa = $req->empresa;

        $contacto->save();

        $data = [
            'contacto' => $contacto,
            'status'=> 200
        ];

        return response()->json($data,200);
    }
    
    public function updatePartial(Request $req, $id){
        
        $contacto = ContactoModel::find($id);

        if(!$contacto){
            $data = [
                "message" => "No se encontro el usuario",
                "status" => 400,
            ];
            
            return response()->json($data, 400);
        }

        if($req->nombre) $contacto->nombre = $req->nombre;
        if($req->apellido) $contacto->apellido = $req->apellido;
        if($req->empresa) $contacto->empresa = $req->empresa;

        $contacto->save();

        $data = [
            'contacto' => $contacto,
            'status'=> 200
        ];

        return response()->json($data,200);
    }

    public function delete($id){
        $contacto = ContactoModel::find($id);

        if(!$contacto){
            $data = [
                "message" => "El usuario no existe o ya ha sido eliminado.",
                "status" => 404,
            ];
            
            return response()->json($data, 404);
        }

        $contacto->delete();

        $data = [
            "message" => "El usuario fue eliminado",
            "status" => 200,
        ];
        
        return response()->json($data, 200);
    }

}
