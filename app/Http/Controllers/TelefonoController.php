<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\TelefonoModel;
use App\Models\ContactoModel;

class TelefonoController extends Controller
{
    public function create(Request $req){

        // $contacto = ContactoModel::find($req->contacto_id);
        
        $validator = Validator::make($req->all(), [
            'telefono'=>'max:255|required',
            'lada'=>'max:255',
            'tipo'=>'max:255',
            'contacto_id'=>'required'
        ]);

        $tel = TelefonoModel::create([
            'telefono'=>$req->telefono,
            'lada'=>$req->lada ?? '+52',
            'tipo'=>$req->tipo ?? 'Personal',
            'contacto_id'=>$req->contacto_id
        ]);

        if(!$tel){
            $data = [
                "message"=>"Error al crear telefono",
                "status"=>500
            ];

            return response()->json($data,500);
        }

        $data = [
            "contacto"=>$tel,
            "status"=>200
        ];

        return response()->json($data,200);
    }
}
