<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\ContactoModel;
use App\Http\Controllers\ContactoController;
use App\Http\Controllers\TelefonoController;

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');

Route::post('/contacto', [ContactoController::class, "create"]);
Route::post('/telefono', [TelefonoController::class, "create"]);
Route::get('/contacto', [ContactoController::class, "readAll"]);
Route::get('/contacto_paged', [ContactoController::class, "readAllPaged"]);
Route::get('/contacto/{id}', [ContactoController::class, "read"]);
Route::put('/contacto/{id}', [ContactoController::class, "update"]);
Route::patch('/contacto/{id}', [ContactoController::class, "updatePartial"]);
Route::delete('/contacto/{id}', [ContactoController::class, "delete"]);